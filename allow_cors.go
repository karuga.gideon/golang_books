package main

import(
  "fmt"
  "net/http"

  "github.com/gorilla/handlers"
  "github.com/gorilla/mux"
)

const port string = "8089"

func index(w http.ResponseWriter, r *http.Request){
  w.Write([]byte("Hello World!"))
}

func main(){

  router := mux.NewRouter()
  headers := handlers.AllowedHeaders([]string{"X-Requested-With","Content-Type","Authorization"})
  methods := handlers.AllowedMethods([]string{"GET","POST","PUT","DELETE"})
  origins := handlers.AllowedHeaders([]string{"*"})

  router.HandleFunc("/", index).Methods("GET")

  fmt.Println("Server started on port : "+port)
  
  http.ListenAndServe(":"+port, handlers.CORS(headers, methods, origins)(router))


}
