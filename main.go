package main

import (
  "fmt"
  "encoding/json"
  "log"
  "net/http"
  "github.com/gorilla/mux"
  "math/rand"
  "time"
  "strconv"
  "database/sql"
   _ "github.com/go-sql-driver/mysql"
)


// Book Struct / Our Model
type Book struct {
  ID      string `json:"id"`
  Isbn    string `json:"isbn"`
  Title   string `json:"title"`
  Author  *Author `json:"author"`
}

// Author Struct
type Author struct {
  Firstname    string `json:"firstname"`
  Lastname     string `json:"lastname"`
  Phone        string `json:"phone"`
  Email        string `json:"email"`
}

// Book Struct / Our Model
type Article struct {
  Id      string `json:"id"`
  Title   string `json:"title"`
  Author  string `json:"author"`
  Body    string `json:"body"`
}

// Init Books variable as slice Book Struct
var books []Book
var articles []Article

// Get all books -- /api/books
func getBooks(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")
  json.NewEncoder(w).Encode(books)
}

func ussdTest(w http.ResponseWriter, r *http.Request)(result string){
  // Get all books -- /api/books
  // func getBooks(w http.ResponseWriter, r *http.Request){
    // w.Header().Set("Content-Type", "application/json")
    // json.NewEncoder(w).Encode(books)
    return "Listing books..."
  // }
}

// Get single book -- /api/book/{id}
func getBook(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")
  params := mux.Vars(r) // Get params

  // loop through books and find with id
  for _, item := range books {
    if item.ID == params["id"] {
      json.NewEncoder(w).Encode(item)
      return
    }
  }
  json.NewEncoder(w).Encode(&Book{})
}

// Create book  -- /api/books
func createBook(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")
  var book Book
  _= json.NewDecoder(r.Body).Decode(&book)
  book.ID = strconv.Itoa(rand.Intn(10000000)) // Mock ID - Not Safe
  books = append(books, book)
  json.NewEncoder(w).Encode(book)
}

// Get all articles -- /api/articles [GET]  -- from db
func getArticles(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")

  // Connect to database
  db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/jobs_center")
  checkError(err)

  // Get data from db
  results, err := db.Query("SELECT * FROM articles")
  checkError(err)

  for results.Next(){

    var id  int
    var title string
    var author string
    var body string
    var create_date string
    var intrash string

    err = results.Scan(&id, &title, &author, &body, &create_date, &intrash)
    checkError(err)

    fmt.Println(title)

    var article Article = Article{strconv.Itoa(id), title, author, body}
    articles = append(articles, article)

  }

  defer results.Close()

  json.NewEncoder(w).Encode(articles)
}

// Create an article - saving it to database  -- /api/articles [POST]
func createArticle(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")

  var article Article
  _= json.NewDecoder(r.Body).Decode(&article)
  articles = append(articles, article)

  // Connect to database
  db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/jobs_center")
  checkError(err)

  // insert data to db
  insert, err := db.Query("INSERT INTO articles (title, author, body) VALUES ('"+article.Title+"','"+article.Author+"','"+article.Body+"') ")
  checkError(err)

  defer insert.Close()
  fmt.Println("Successfully inserted data to database.")


  json.NewEncoder(w).Encode(article)
}

// Update article  -- /api/articles [POST]
func updateArticle(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")

  var article Article
  _= json.NewDecoder(r.Body).Decode(&article)

  // Connect to database
  db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/jobs_center")
  checkError(err)

  // insert data to db
  insert, err := db.Query("UPDATE articles set title = '"+article.Title+"', author = '"+article.Author+"', body = '"+article.Body+"' WHERE id = '"+article.Id+"' ")
  checkError(err)

  defer insert.Close()
  fmt.Println("Successfully updated article.")


  json.NewEncoder(w).Encode(article)
}

// Update book -- /api/book/{id}
func updateBook(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")
  params := mux.Vars(r) // Get params
  for index, item := range books {
    if item.ID == params["id"]{
      books = append(books[:index], books[index+1:]...)
      var book Book
      _= json.NewDecoder(r.Body).Decode(&book)
      book.ID = params["id"]
      books = append(books, book)
      json.NewEncoder(w).Encode(book)
      return
    }
  }
  json.NewEncoder(w).Encode(books)
}

// Delete Book -- /api/book/{id}
func deleteBook(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Content-Type", "application/json")
  params := mux.Vars(r) // Get params
  for index, item := range books {
    if item.ID == params["id"]{
      books = append(books[:index], books[index+1:]...)
      break
    }
  }
  json.NewEncoder(w).Encode(books)
}

func connectToDatabase(){
  // Connect to database
  db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/jobs_center")
  checkError(err)

  defer db.Close()
}

func checkError(err error){
  if err != nil {
    panic(err.Error())
  }
}

func main(){

  fmt.Println("Hello Books - MySql Rest API started at : ", time.Now())

  connectToDatabase()
  fmt.Println("Successfully connected to MySql database.")

  // Init max router
  router := mux.NewRouter()

  // Mock data - @todo - implement database
  books = append(books, Book{ID:"1", Isbn:"989FADSFU9", Title:"The Wizard of Oz",
    Author: &Author{Firstname: "Gideon", Lastname: "Karuga", Phone:"25479980989", Email:"karuga.gideon@gmail.com"} })
  books = append(books, Book{ID:"2", Isbn:"FASD899890", Title:"The Invisible Man",
    Author: &Author{Firstname: "Jane", Lastname: "Doe", Phone:"25479980989", Email:"jd@gmail.com"} })
  books = append(books, Book{ID:"3", Isbn:"FASS08FDSF", Title:"Saving America",
    Author: &Author{Firstname: "John", Lastname: "Doe", Phone:"25479980989", Email:"johnd@gmail.com"} })

  // Route handlers / Endpoints - for Books
  router.HandleFunc("/api/books", getBooks).Methods("GET")
  router.HandleFunc("/api/book/{id}", getBook).Methods("GET")
  router.HandleFunc("/api/books", createBook).Methods("POST")
  router.HandleFunc("/api/book/{id}", updateBook).Methods("PUT")
  router.HandleFunc("/api/book/{id}", deleteBook).Methods("DELETE")

  // Artile Endpoints
  router.HandleFunc("/api/articles", getArticles).Methods("GET")
  router.HandleFunc("/api/articles", createArticle).Methods("POST")
  router.HandleFunc("/api/article/{id}", updateArticle).Methods("PUT")

  // ussdTest
  router.HandleFunc("/api/ussd/", ussdTest).Methods("GET")

  // Run the server
  log.Fatal(http.ListenAndServe(":9022", router))

}


// mux
// go get -u github.com/gorilla/mux

// - Run a go application
// go run main.go
