package main

import (
  "fmt"
  "net/http"
  "os"
  "database/sql"
  _"github.com/bmizerany/pq"
  sharedF "restapi/utils"
)

// ussd session for each request
type ussd_session struct {
  msisdn       string
  session_id   string
  ussd_input   string
  is_logged_in bool
  user_id      uint64
}

// Db Connectivity
const (
	DB_USER = "pgadmin"
	DB_PASSWORD = "openpgpwd"
	DB_NAME = "python_apps"
	DB_PORT = "5432"
	DB_HOST = "localhost"
)
// dbInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME )
var db *sql.DB
// home, handle all the USSD requests.
func index_handler(w http.ResponseWriter, r *http.Request){
  // get msisdn, session id, and last ussd input param
  user_ussd_session :=  get_ussd_params(r)

  fmt.Println("Processing request for >>> ", user_ussd_session.msisdn, "Param selected : ", user_ussd_session.ussd_input);

  write_to_file(user_ussd_session)
  // user_ussd_session.write_to_file()

  switch(user_ussd_session.ussd_input){
    case "1":
      fmt.Fprintf(w, "Welcome to USSD.\nEnter PIN\n")
    case "2":
      fmt.Fprintf(w, "Welcome to USSD.\nEnter First Name\n")
    default:
      fmt.Fprintf(w, "Welcome to our USSD.\n1.Register\n2.Login")
  }

}

// Get all ussd params
func get_ussd_params(r *http.Request) ussd_session {

  fmt.Println("GET params were:", r.URL.Query());

  user_ussd_session := ussd_session {
    msisdn: r.URL.Query().Get("MSISDN"),
    session_id: r.URL.Query().Get("SESSION_ID"),
    ussd_input: r.URL.Query().Get("USSD_PARAMS"),
    is_logged_in: false }

    return user_ussd_session

}

// Write USSD Sessions to File
func write_to_file(class ussd_session){

    fmt.Printf("%+v\n", class)

    fmt.Println("Creating file >>> ", class.session_id)
    session_file := "sessions/"+class.session_id+".txt"
    f, err := os.Create(session_file)
    check(err)
    defer f.Close()

    // Write as string
    // user_ussd_session_json, _ := json.Marshal(class)
    user_ussd_session_json := "{session_id:"+class.session_id+", msisdn:"+class.msisdn+"}"
    n3, err := f.WriteString(user_ussd_session_json+"\n")
    check(err)
    fmt.Printf("wrote %d bytes\n", n3)
    f.Sync()

}

// File error handling
func check(e error) {
    if e != nil {
        panic(e)
    }
}

// Test USSD Status - whether it's up or not
func status_handler(w http.ResponseWriter, r *http.Request){
  fmt.Fprintf(w, "USSD is up and running.")
}

func get_users(){

  fmt.Println("Getting users take 1...")

  // Get data from db
  results, err := db.Query("SELECT id, username, email, password, intrash FROM py_users")
  sharedF.CheckError(err)

  for results.Next(){

    var id  int
    var username string
    var email string
    var password string
    var intrash string

    // err = results.Scan(&id, &username, &email, &password, &intrash)
    err = results.Scan(&id, &username, &email, &password, &intrash)
    sharedF.CheckError(err)

    fmt.Println(email)
  }
}

// Entry point to class and application
func main(){
  fmt.Println(sharedF.Add(7,3))

  // db connectivity
  // connStr := "postgres://pgadmin:openpgpwd@localhost/python_apps"
  // db, err := sql.Open("postgres", connStr)
  var err error
  // db, err = sql.Open("postgres", connStr)
  db, err = sql.Open("postgres", "dbname=python_apps user=pgadmin password=openpgpwd port=5432 sslmode=disable")
  sharedF.CheckError(err)
  fmt.Println("You are now connected to the db.")

  get_users()

	// id := 1
	// rows, err := db.Query("SELECT email FROM user WHERE id = $1", id)
  // fmt.Println(rows)

  // Get data from db
  results, err := db.Query("SELECT id, username, email, password, intrash FROM py_users")
  sharedF.CheckError(err)

  for results.Next(){

    var id  int
    var username string
    var email string
    var password string
    var intrash string

    // err = results.Scan(&id, &username, &email, &password, &intrash)
    err = results.Scan(&id, &username, &email, &password, &intrash)
    sharedF.CheckError(err)

    fmt.Println(email)

    // var article Article = Article{strconv.Itoa(id), title, author, body}
    // articles = append(articles, article)

  }

  http.HandleFunc("/", index_handler)
  http.HandleFunc("/status/", status_handler)
  http.ListenAndServe(":8080", nil)
}
